#include "HttpRequest.h"

int HttpRequest::_idLast = 0;
// std::vector<int> HttpRequest::_freeIdList;

HttpRequest::HttpRequest() : _addres("localhost:1280"), _method("GET"), _contentType("text/plain"), _body("{}"), _id(-1)
{
}

HttpRequest::~HttpRequest()
{
    // if(_id != -1) _freeIdList.push_back(_id);
}

void HttpRequest::createId()
{
    _id = generateId();
}

int HttpRequest::getId()
{
    return _id;
}

void HttpRequest::bind()
{
    // We are not using it in squirrel atm...

    /*
    using namespace SqModule;

    Sqrat::Class<HttpRequest> httpRequest(vm, "httpClient_Request");

    httpRequest.Prop("addres", &HttpRequest::getAddres);
    httpRequest.Prop("method", &HttpRequest::getMethod);
    httpRequest.Prop("content_type", &HttpRequest::getContentType);
    httpRequest.Prop("body", &HttpRequest::getBody);
    */
}

void HttpRequest::setAddres(const std::string& addres)
{
    _addres = addres;
}

const std::string& HttpRequest::getAddres() const
{
    return _addres;
}

void HttpRequest::setMethod(const std::string& method)
{
    _method = method;
}

const std::string& HttpRequest::getMethod() const
{
    return _method;
}

void HttpRequest::setContentType(const std::string& contentType)
{
    _contentType = contentType;
}

const std::string& HttpRequest::getContentType() const
{
    return _contentType;
}

void HttpRequest::setBody(const std::string& body)
{
    _body = body;
}

const std::string& HttpRequest::getBody() const
{
    return _body;
}

void HttpRequest::setHeaders(const nlohmann::json& headers)
{
    _headers = headers;
}

const nlohmann::json& HttpRequest::getHeaders() const
{
    return _headers;
}

int HttpRequest::generateId()
{
    return _idLast += 1;
}
