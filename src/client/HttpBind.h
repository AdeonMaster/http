#pragma once

#include <sqrat.h>
#include <json.h>

namespace HttpBind
{
    void addEvent(const char* eventName);
    void bind();

    // HttpClient
    SQInteger get(HSQUIRRELVM vm);
    SQInteger del(HSQUIRRELVM vm);
    SQInteger post(HSQUIRRELVM vm);
    SQInteger put(HSQUIRRELVM vm);
}
