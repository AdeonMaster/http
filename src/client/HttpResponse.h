#pragma once

#include <sqrat.h>
#include <httplib.h>

class HttpResponse
{
public:
    int _id;

    int _status;
    httplib::Headers _headers;
    std::string _body;

public:
    HttpResponse(int id = -1);
    ~HttpResponse();

    int getId();

public:
    static void bind();

    void setStatus(int newStatus);

    const int getStatus() const;

    void setHeaders(const httplib::Headers& headers);

    const Sqrat::Table getTransformedHeaders() const;

    void setBody(const std::string& newBody);

    const std::string& getBody() const;
};