#include "HttpClient.h"

#include <vector>
#include <map>
#include <iostream>

HttpClient* HttpClient::_obj = nullptr;

HttpClient::HttpClient() : _requestQueue { 100 }, _responseQueue { 100 }, _clientPool {}
{
    _thread = std::make_unique<std::thread>(&HttpClient::requestListener, this);
}

void HttpClient::pushRequest(HttpRequest request)
{
    _requestQueue.emplace(request);
}

void HttpClient::requestListener()
{
    HttpRequest request;

    while (true)
    {
        if (_requestQueue.try_dequeue(request))
        {
            std::string host, path;

            splitUrl(request.getAddres(), host, path);

            if (host.length() == 0)
                continue;

            try
            {
                if (_clientPool.find(host) == _clientPool.end()) 
                {
                    _clientPool[host] = std::make_shared<httplib::SSLClient>(host.c_str());
                   
                    if (_clientPool.size() > 5)
                    {
                        auto last = _clientPool.begin();
                        last->second->stop();
                        
                        _clientPool.erase(last);
                    }
                }

                httplib::SSLClient& client = *_clientPool[host];
                
                const std::string method = request.getMethod();
                const std::string body = request.getBody();
                const std::string contentType = request.getContentType();
                const nlohmann::json headers = request.getHeaders();

                httplib::Headers header;

                for (auto it = headers.begin(); it != headers.end(); ++it) 
                {
                    auto key = it.key();
                    auto value = it.value();

                    if (value.is_string())
                    {
                        header.emplace(key.c_str(), value);
                    }
                    else 
                        std::cout << "HttpClient: " << "wrong header value" << std::endl;
                }

                client.enable_server_certificate_verification(false);
                client.set_follow_location(true);

                auto moduleResponse = (method == "GET") ? client.Get(path, header)
                    : (method == "POST") ? client.Post(path, header, body, contentType)
                    : (method == "DELETE") ? client.Delete(path, header, body, contentType)
                    : client.Put(path, header, body, contentType);

                HttpResponse response = HttpResponse(request.getId());

                if (moduleResponse)
                {
                    response.setStatus(moduleResponse->status);
                    response.setHeaders(moduleResponse->headers);
                    response.setBody(moduleResponse->body);
                }
                else 
                    response.setStatus(444);

                _responseQueue.enqueue(response);
            }
            catch (std::exception const err)
            {
                std::cout << err.what() << std::endl;
            }
        }
     
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
    }
}

void HttpClient::responseListener()
{
    if (_responseQueue.size_approx() == 0)
        return;

    HttpResponse response;

    while (_responseQueue.try_dequeue(response))
    {
       Sqrat::RootTable().GetFunction("callEvent").Execute("httpClient:onResponse", response);
    }
}

void HttpClient::splitUrl(const std::string& url, std::string& host, std::string& path) 
{
    size_t protocolEndPos = url.find("://");
    protocolEndPos = (protocolEndPos != std::string::npos) ? protocolEndPos + 3 : 0;

    size_t pathStartPos = url.find('/', protocolEndPos);

    if (pathStartPos != std::string::npos) 
    {
        host = url.substr(0, pathStartPos);
        path = url.substr(pathStartPos);
    }
    else 
    {
        host = url;
        path = "/";
    }
}

HttpClient* HttpClient::getInstance()
{
    if (_obj == nullptr)
    {
        _obj = new HttpClient();
    }

    return _obj;
}
