#include "HttpBind.h"

#include "HttpClient.h"
#include "HttpRequest.h"

void HttpBind::addEvent(const char* eventName)
{
    Sqrat::Function sq_addEvent = Sqrat::RootTable().GetFunction("addEvent");

    if (sq_addEvent.IsNull())
        return;

    sq_addEvent(eventName);
}

void HttpBind::bind()
{
    using namespace SqModule;

    /* squirreldoc (event)
    *
    * This event is triggered every time when client get response to http request.
    *
    * @version	0.0.0
    * @side		shared
    * @name		httpClient:onResponse
    * @category client
    * @param	(object) httpResponse Object, whitch contains return information about client request.
    *
    */

    addEvent("httpClient:onResponse");

    /* squirreldoc (class)
    *
    * This class represents httpClient namespace.
    *
    * @version	0.0.0
    * @side		shared
    * @name		httpClient
    * @category client
    *
    */
    
    Sqrat::RootTable().Bind("httpClient", Sqrat::Table(vm)
        .SquirrelFunc("get", &HttpBind::get)
        .SquirrelFunc("delete", &HttpBind::del)
        .SquirrelFunc("post", &HttpBind::post)
        .SquirrelFunc("put", &HttpBind::put));
}

/* squirreldoc (method)
*
* This function is used to send GET request.
*
* @version	0.0.0
* @name		get
* @param	(string) addres Your request addres, use the address without the https or http prefixes.
* @param	(table or null) headers Your headers. This parametr is optional.
* @return	(int) Returns your request ID, later you can use this ID to identify your response in httpClient:onResponse callback.
*
*/

SQInteger HttpBind::get(HSQUIRRELVM vm)
{
    SQInteger numArgs = sq_gettop(vm);

    if (numArgs < 2 || numArgs > 3)
        return sq_throwerror(vm, "wrong number of parameters");

    HttpRequest request = HttpRequest();

    if (numArgs == 3 && sq_gettype(vm, 3) == OT_TABLE)
    {
        nlohmann::json object = nlohmann::json::object();
        pjson::fillJSONObject(object);

        request.setHeaders(object);
    }

    const SQChar* addr;
    sq_getstring(vm, 2, &addr);

    request.setAddres(addr);
    request.setMethod("GET");
    request.createId();

    HttpClient::getInstance()->pushRequest(request);

    Sqrat::PushVar(vm, request.getId());
    return 1;
}

/* squirreldoc (method)
*
* This function is used to send DELETE request.
*
* @version	0.0.0
* @name		delete
* @param	(string) addres Your request addres, use the address without the https:// or http:// prefixes.
* @param	(string) contentType Your request content-type.
* @param	(string) body Your request body, in most case your body is going to be JSON type.
* @param	(table or null) headers Your headers. This parametr is optional.
* @return	(int) Returns your request ID, later you can use this ID to identify your response in httpClient:onResponse callback.
*
*/

SQInteger HttpBind::del(HSQUIRRELVM vm)
{
    SQInteger numArgs = sq_gettop(vm);

    if (numArgs < 4 || numArgs > 5)
        return sq_throwerror(vm, "wrong number of parameters");

    HttpRequest request = HttpRequest();

    if (numArgs == 5 && sq_gettype(vm, 5) == OT_TABLE)
    {
        nlohmann::json object = nlohmann::json::object();
        pjson::fillJSONObject(object);

        request.setHeaders(object);
    }

    const SQChar* body;
    sq_getstring(vm, 4, &body);

    const SQChar* content;
    sq_getstring(vm, 3, &content);

    const SQChar* addr;
    sq_getstring(vm, 2, &addr);

    request.setAddres(addr);
    request.setMethod("DELETE");
    request.setContentType(content);
    request.setBody(body);
    request.createId();

    HttpClient::getInstance()->pushRequest(request);

    Sqrat::PushVar(vm, request.getId());
    return 1;
}

/* squirreldoc (method)
*
* This function is used to send POST request.
*
* @version	0.0.0
* @name		post
* @param	(string) addres Your request addres, use the address without the https:// or http:// prefixes.
* @param	(string) contentType Your request content-type.
* @param	(string) body Your request body, in most case your body is going to be JSON type.
* @param	(table or null) headers Your headers. This parametr is optional.
* @return	(int) Returns your request ID, later you can use this ID to identify your response in httpClient:onResponse callback.
*
*/

SQInteger HttpBind::post(HSQUIRRELVM vm)
{
    SQInteger numArgs = sq_gettop(vm);

    if (numArgs < 4 || numArgs > 5)
        return sq_throwerror(vm, "wrong number of parameters");

    HttpRequest request = HttpRequest();

    if (numArgs == 5 && sq_gettype(vm, 5) == OT_TABLE)
    {
        nlohmann::json object = nlohmann::json::object();
        pjson::fillJSONObject(object);

        request.setHeaders(object);
    }

    const SQChar* body;
    sq_getstring(vm, 4, &body);

    const SQChar* content;
    sq_getstring(vm, 3, &content);

    const SQChar* addr;
    sq_getstring(vm, 2, &addr);

    request.setAddres(addr);
    request.setMethod("POST");
    request.setContentType(content);
    request.setBody(body);
    request.createId();

    HttpClient::getInstance()->pushRequest(request);

    Sqrat::PushVar(vm, request.getId());
    return 1;
}

/* squirreldoc (method)
*
* This function is used to send PUT request.
*
* @version	0.0.0
* @name		put
* @param	(string) addres Your request addres, use the address without the https:// or http:// prefixes.
* @param	(string) contentType Your request content-type.
* @param	(string) body Your request body, in most case your body is going to be JSON type.
* @param	(table or null) headers Your headers. This parametr is optional.
* @return	(int) Returns your request ID, later you can use this ID to identify your response in httpClient:onResponse callback.
*
*/

SQInteger HttpBind::put(HSQUIRRELVM vm)
{
    SQInteger numArgs = sq_gettop(vm);

    if (numArgs < 4 || numArgs > 5)
        return sq_throwerror(vm, "wrong number of parameters");

    HttpRequest request = HttpRequest();

    if (numArgs == 5 && sq_gettype(vm, 5) == OT_TABLE)
    {
        nlohmann::json object = nlohmann::json::object();
        pjson::fillJSONObject(object);

        request.setHeaders(object);
    }

    const SQChar* body;
    sq_getstring(vm, 4, &body);

    const SQChar* content;
    sq_getstring(vm, 3, &content);

    const SQChar* addr;
    sq_getstring(vm, 2, &addr);

    request.setAddres(addr);
    request.setMethod("PUT");
    request.setContentType(content);
    request.setBody(body);
    request.createId();

    HttpClient::getInstance()->pushRequest(request);

    Sqrat::PushVar(vm, request.getId());
    return 1;
}