#include "HttpResponse.h"

HttpResponse::HttpResponse(int id) : _id(id), _status(404), _body("{}")
{
}

HttpResponse::~HttpResponse()
{
}

void HttpResponse::bind()
{
    using namespace SqModule;

    /* squirreldoc (class)
    *
    * This class represents http response, is provided only by response callback, values of her objects cannot be modified and containts informations about response.
    *
    * @version	0.0.0
    * @side		shared
    * @name		httpResponse
    * @category client
    *
    */

    Sqrat::Class<HttpResponse> httpResponse(vm, "httpClient_Response");

    httpResponse.Prop("status", &HttpResponse::getStatus);
    httpResponse.Prop("headers", &HttpResponse::getTransformedHeaders);
    httpResponse.Prop("body", &HttpResponse::getBody);
    httpResponse.Prop("id", &HttpResponse::getId);
}

/* squirreldoc (property)
*
* Returns response status code.
*
* @version	0.0.0
* @name     status
* @return   (int) Response status code.
*
*/

void HttpResponse::setStatus(int newStatus)
{
    _status = newStatus;
}

const int HttpResponse::getStatus() const
{
    return _status;
}

/* squirreldoc (property)
*
* Returns response headers.
*
* @version  0.0.0
* @name     headers
* @return   (table) Response headers.
*
*/

void HttpResponse::setHeaders(const httplib::Headers& headers)
{
    _headers = headers;
}

// Squirrel thread
const Sqrat::Table HttpResponse::getTransformedHeaders() const
{
    Sqrat::Table sqTable(SqModule::vm);

    for (auto& header : _headers) {
        sqTable.SetValue(header.first.c_str(), header.second.c_str());
    }

    return sqTable;
}

/* squirreldoc (property)
*
* Returns request body content.
*
* @version  0.0.0
* @name     body
* @return   (string) Response body, formatted as text/JSON, depending on used API it`s possible to transform body into array/table.
*
*/

void HttpResponse::setBody(const std::string& newBody)
{
    _body = newBody;
}

const std::string& HttpResponse::getBody() const
{
    return _body;
}

/* squirreldoc (property)
*
* Returns response id number.
*
* @version  0.0.0
* @name     id
* @return   (int) Response ID. This ID can be used to link your response with the request. Request functions always return an ID number.
*
*/

int HttpResponse::getId()
{
    return _id;
}