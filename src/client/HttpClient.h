#pragma once

#define CPPHTTPLIB_OPENSSL_SUPPORT

#include <httplib.h>
#include <thread>
#include <readerwriterqueue.h>

#include "HttpRequest.h"
#include "HttpResponse.h"

class HttpClient
{
private:
    moodycamel::ReaderWriterQueue<HttpRequest> _requestQueue;
    moodycamel::ReaderWriterQueue<HttpResponse> _responseQueue;

    std::map<std::string, std::shared_ptr<httplib::SSLClient>> _clientPool;

    std::unique_ptr<std::thread> _thread;

public:
    HttpClient();
    static HttpClient* _obj;

public:
    static void splitUrl(const std::string& url, std::string& host, std::string& path);

    void pushRequest(HttpRequest request);

    void requestListener();
    void responseListener();

    static HttpClient* getInstance();
};
