#define WIN32_LEAN_AND_MEAN

#include "HttpClient.h"
#include "HttpBind.h"

#include "sq_callback.h"

#include <json.h>
#include <sqrat.h>

void addEventHandler(const char* eventName, SQFUNCTION closure, unsigned int priority = 9999)
{
    using namespace SqModule;

    Sqrat::Function sq_addEventHandler = Sqrat::RootTable().GetFunction("addEventHandler");

    if (sq_addEventHandler.IsNull())
        return;

    HSQOBJECT closureHandle;

    sq_newclosure(vm, closure, 0);
    sq_getstackobj(vm, -1, &closureHandle); 

    Sqrat::Function func(vm, Sqrat::RootTable().GetObject(), closureHandle);
    sq_addEventHandler(eventName, func, priority);

    sq_pop(vm, 1);
}

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
    SqModule::Initialize(vm, api);
    Sqrat::DefaultVM::Set(vm);

    Sqrat::Object serverSide = Sqrat::ConstTable(vm).GetSlot("SERVER_SIDE");
    
    if (!serverSide.IsNull())
    {
        addEventHandler("onTick", sq_onTick, 0xFFFFFFFF);
    }
    else
    {
        addEventHandler("onRender", sq_onTick, 0xFFFFFFFF);
    }

    HttpClient::getInstance();

    HttpBind::bind();
    // Response/request
    HttpResponse::bind();
    HttpRequest::bind();

    pjson::bind();

    return SQ_OK;
}
