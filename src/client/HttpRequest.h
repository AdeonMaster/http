#pragma once

#include <iostream>
#include <vector>
#include <json.h>

class HttpRequest
{
public:
    int _id;

    std::string _path;

    std::string _addres;
    std::string _method;

    std::string _contentType;
    std::string _body;

    nlohmann::json _headers;

public:
    static int _idLast;
    // static std::vector<int> _freeIdList;

public:
    HttpRequest();
    ~HttpRequest();

    void createId();

    int getId();

    static void bind();

    void setAddres(const std::string& addres);

    const std::string& getAddres() const;

    void setMethod(const std::string& method);

    const std::string& getMethod() const;

    void setContentType(const std::string& contentType);

    const std::string& getContentType() const;

    void setBody(const std::string& body);

    const std::string& getBody() const;

    void setHeaders(const nlohmann::json& headers);

    const nlohmann::json& getHeaders() const;

private:
    static int generateId();
};