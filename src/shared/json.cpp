#include "json.h"

#include <iostream>

void pjson::bind()
{
	using namespace SqModule;

	Sqrat::Object loaded = Sqrat::RootTable(vm).GetSlot("pjson");

	if (loaded.IsNull())
	{
		/* squirreldoc (class)
		*
		* This class represents pjson namespace.
		*
		* @version	0.0.0
		* @side		shared
		* @name		pjson
		*
		*/

		Sqrat::RootTable().Bind("pjson", Sqrat::Table(vm)
			.SquirrelFunc("packJSON", &pjson::packJSON, 2)
			.SquirrelFunc("unpackJSON", &pjson::unpackJSON, 2));
	}
}

/* squirreldoc (method)
*
* Transforms given value into JSON object.
*
* @version  0.0.0
* @static
* @name     packJSON
* @param    (string) Value to transform, types possible to transform are: array, table.
* @return   (json) JSON object.
*
*/

SQInteger pjson::packJSON(HSQUIRRELVM vm)
{	
	switch (sq_gettype(vm, 2))
	{
	case OT_BOOL:
	{
		SQBool value;
		sq_getbool(vm, 2, &value);

		Sqrat::PushVar(vm, value);
		break;
	}

	case OT_INTEGER:
	{
		SQInteger value;
		sq_getinteger(vm, 2, &value);

		Sqrat::PushVar(vm, value);
		break;
	}

	case OT_FLOAT:
	{
		SQFloat value;
		sq_getfloat(vm, 2, &value);

		Sqrat::PushVar(vm, value);
		break;
	}

	case OT_STRING:
	{
		const SQChar* value;
		sq_getstring(vm, 2, &value);

		Sqrat::PushVar(vm, value);
		break;
	}

	case OT_ARRAY:
	{
		nlohmann::json array = nlohmann::json::array();
		pjson::fillJSONArray(array);

		Sqrat::PushVar(vm, array.dump());
		break;
	}

	case OT_TABLE:
	{
		nlohmann::json object = nlohmann::json::object();
		pjson::fillJSONObject(object);

		Sqrat::PushVar(vm, object.dump());
		break;
	}

	default:
		return sq_throwerror(vm, "(pjson::packJSON) wrong type of parameters expecting bool|int|float|string|array|table");
	}

	return 1;
}

/* squirreldoc (method)
*
* Transforms given JSON object into squirrel array/table.
*
* @version  0.0.0
* @static
* @name     unpackJSON
* @param    (json) Value to transform into squirrel type, should be JSON type object.
* @return   (array/table) Squirrel type object.
*
*/

SQInteger pjson::unpackJSON(HSQUIRRELVM vm)
{
	if (sq_gettype(vm, 2) == OT_STRING)
	{
		const SQChar* value;
		sq_getstring(vm, 2, &value);

		if (value == nullptr || std::strlen(value) == 0)
			return 0;

		nlohmann::json data = nlohmann::json::parse(value);

		switch (data.type())
		{
			default:
				sq_pushnull(vm);
				break;

			case nlohmann::json::value_t::boolean:
				Sqrat::PushVar(vm, data.get<bool>());
				break;

			case nlohmann::json::value_t::number_integer:
			case nlohmann::json::value_t::number_unsigned:
				Sqrat::PushVar(vm, data.get<SQInteger>());
				break;

			case nlohmann::json::value_t::number_float:
				Sqrat::PushVar(vm, data.get<SQFloat>());
				break;

			case nlohmann::json::value_t::string:
				Sqrat::PushVar(vm, data.get<Sqrat::string>());
				break;

			case nlohmann::json::value_t::array:
			{
				Sqrat::Array array(vm);
				fillSqratArray(data, array);

				Sqrat::PushVar(vm, array);
				break;
			}

			case nlohmann::json::value_t::object:
			{
				Sqrat::Table table(vm);
				fillSqratTable(data, table);

				Sqrat::PushVar(vm, table);
				break;
			}
		}
	}
	else 
		return sq_throwerror(vm, "(pjson::unpackJSON) wrong type of parameters expecting string");

	return 1;
}

void pjson::fillJSONArray(nlohmann::json& array)
{
	using namespace SqModule;

	sq_pushnull(vm);

	while (SQ_SUCCEEDED(sq_next(vm, -2)))
	{
		SQInteger key;
		sq_getinteger(vm, -2, &key);

		switch (sq_gettype(vm, -1))
		{
		case OT_BOOL:
		{
			SQBool b;
			sq_getbool(vm, -1, &b);

			array.push_back(b == SQTrue);
			break;
		}

		case OT_INTEGER:
		{
			SQInteger i;
			sq_getinteger(vm, -1, &i);

			array.push_back(i);
			break;
		}

		case OT_FLOAT:
		{
			SQFloat f;
			sq_getfloat(vm, -1, &f);

			array.push_back(f);
			break;
		}

		case OT_STRING:
		{
			const SQChar* s;
			sq_getstring(vm, -1, &s);

			array.push_back(Sqrat::string(s));
			break;
		}

		case OT_ARRAY:
		{
			nlohmann::json nestedArray = nlohmann::json::array();

			fillJSONArray(nestedArray);
			array.push_back(nestedArray);
			break;
		}

		case OT_TABLE:
		{
			nlohmann::json nestedObject = nlohmann::json::object();

			fillJSONObject(nestedObject);
			array.push_back(nestedObject);
			break;
		}
		}

		sq_pop(vm, 2);
	}

	sq_pop(vm, 1);
}

void pjson::fillJSONObject(nlohmann::json& object)
{
	using namespace SqModule;

	sq_pushnull(vm);

	while (SQ_SUCCEEDED(sq_next(vm, -2)))
	{
		const SQChar* key;
		sq_getstring(vm, -2, &key);

		switch (sq_gettype(vm, -1))
		{
		case OT_BOOL:
		{
			SQBool b;
			sq_getbool(vm, -1, &b);

			object[key] = (b == SQTrue);
			break;
		}

		case OT_INTEGER:
		{
			SQInteger i;
			sq_getinteger(vm, -1, &i);

			object[key] = i;
			break;
		}

		case OT_FLOAT:
		{
			SQFloat f;
			sq_getfloat(vm, -1, &f);

			object[key] = f;
			break;
		}

		case OT_STRING:
		{
			const SQChar* s;
			sq_getstring(vm, -1, &s);

			object[key] = Sqrat::string(s);
			break;
		}

		case OT_ARRAY:
		{
			nlohmann::json nestedArray = nlohmann::json::array();

			fillJSONArray(nestedArray);
			object[key] = nestedArray;
			break;
		}

		case OT_TABLE:
		{
			nlohmann::json nestedObject = nlohmann::json::object();

			fillJSONObject(nestedObject);
			object[key] = nestedObject;
			break;
		}
		}

		sq_pop(vm, 2);
	}

	sq_pop(vm, 1);
}

void pjson::fillSqratArray(nlohmann::json& jsonArray, Sqrat::Array& sqArray)
{
	using namespace SqModule;

	for (auto& value : jsonArray)
	{
		switch (value.type())
		{
		case nlohmann::json::value_t::boolean:
			sqArray.Append(value.get<bool>());
			break;

		case nlohmann::json::value_t::number_integer:
		case nlohmann::json::value_t::number_unsigned:
			sqArray.Append(value.get<SQInteger>());
			break;

		case nlohmann::json::value_t::number_float:
			sqArray.Append(value.get<SQFloat>());
			break;

		case nlohmann::json::value_t::string:
			sqArray.Append(value.get<Sqrat::string>());
			break;

		case nlohmann::json::value_t::array:
		{
			Sqrat::Array nestedSqArray(vm);

			fillSqratArray(value, nestedSqArray);
			sqArray.Append(nestedSqArray);
			break;
		}

		case nlohmann::json::value_t::object:
		{
			Sqrat::Table nestedSqTable(vm);

			fillSqratTable(value, nestedSqTable);
			sqArray.Append(nestedSqTable);
			break;
		}
		}
	}
}

void pjson::fillSqratTable(nlohmann::json& jsonObject, Sqrat::Table& sqTable)
{
	using namespace SqModule;

	for (auto& el : jsonObject.items())
	{
		auto& key = el.key();
		auto& value = el.value();

		switch (el.value().type())
		{
		case nlohmann::json::value_t::boolean:
			sqTable.SetValue(key.c_str(), value.get<bool>());
			break;

		case nlohmann::json::value_t::number_integer:
		case nlohmann::json::value_t::number_unsigned:
			sqTable.SetValue(key.c_str(), value.get<SQInteger>());
			break;

		case nlohmann::json::value_t::number_float:
			sqTable.SetValue(key.c_str(), value.get<SQFloat>());
			break;

		case nlohmann::json::value_t::string:
			sqTable.SetValue(key.c_str(), value.get<Sqrat::string>());
			break;

		case nlohmann::json::value_t::array:
		{
			Sqrat::Array nestedSqArray(vm);

			fillSqratArray(value, nestedSqArray);
			sqTable.SetValue(key.c_str(), nestedSqArray);
			break;
		}

		case nlohmann::json::value_t::object:
		{
			Sqrat::Table nestedSqTable(vm);

			fillSqratTable(value, nestedSqTable);
			sqTable.SetValue(key.c_str(), nestedSqTable);
			break;
		}
		}
	}
}

Sqrat::string pjson::tostring(HSQOBJECT obj)
{
	using namespace SqModule;

	sq_pushobject(vm, obj);

	if (SQ_FAILED(sq_tostring(vm, -1)))
	{
		sq_pop(vm, 1);
		return "";
	}

	const SQChar* cstring = nullptr;
	sq_getstring(vm, -1, &cstring);

	Sqrat::string result = (cstring != nullptr) ? cstring : "";

	sq_pop(vm, 2);
	return result;
}