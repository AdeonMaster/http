#pragma once

#include <sqrat.h>
#include <json.hpp>

namespace pjson
{
	void bind();

	SQInteger packJSON(HSQUIRRELVM vm);
	SQInteger unpackJSON(HSQUIRRELVM vm);

	void fillSqratArray(nlohmann::json& jsonArray, Sqrat::Array& sqArray);
	void fillSqratTable(nlohmann::json& jsonObject, Sqrat::Table& sqTable);
	void fillJSONArray(nlohmann::json& array);
	void fillJSONObject(nlohmann::json& object);

	// Utility function
	Sqrat::string tostring(HSQOBJECT obj);
};
