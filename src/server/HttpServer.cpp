#include "HttpServer.h"

HttpServer* HttpServer::_obj = nullptr;

HttpServer::HttpServer() : _port(7070), _visiblePublic(false), _requestQueue { 50 }, _responseQueue { 50 }
{
}

void HttpServer::requestHanlder(const httplib::Request& request, httplib::Response& response)
{
    HttpRequest squirrelRequest = HttpRequest();

    squirrelRequest.setPath(request.matches[0]);
    squirrelRequest.setAddres(request.remote_addr);
    squirrelRequest.setMethod(request.method);
    squirrelRequest.setHeaders(request.headers);
    squirrelRequest.setBody(request.body);

    _requestQueue.enqueue(squirrelRequest);
    
    while (true) 
    {
        HttpResponse squirrelResponse;

        if (_responseQueue.try_dequeue(squirrelResponse))
        {
            response.status = squirrelResponse.getStatus();
            response.body = squirrelResponse.getBody();
            response.headers = squirrelResponse.getHeaders();

            break;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(5));
    }
}

void HttpServer::responseListener()
{
    if (_requestQueue.size_approx() == 0)
        return;

    HttpRequest request;
   
    while (_requestQueue.try_dequeue(request))
    {
       HttpResponse response = HttpResponse();
       Sqrat::RootTable().GetFunction("callEvent").Execute("httpServer:onRequest", request.getPath().c_str(), request, &response);
       _responseQueue.enqueue(response);
    }
}

void HttpServer::run(const int port, const bool visiblePublic)
{
    if ((port < 0) && (port > 65535))
    {
        std::cout << "Bad port number in HTTP server configuration!" << std::endl;
        return;
    }

    bind_to_port(visiblePublic ? "0.0.0.0" : "127.0.0.1", port);

    std::cout << "-========================================-" << std::endl;
    std::cout << "Initializing HTTP server on port: " << port << "" << std::endl;
    std::cout << "Server visibility: " << (visiblePublic ? "Public" : "Private") << std::endl;
    std::cout << "-========================================-" << std::endl;

    auto hanlder = [&](const httplib::Request& request, httplib::Response& response)
    {
        this->requestHanlder(request, response);
    };

    Get(R"(/.*)", hanlder);
    Delete(R"(/.*)", hanlder);
    Post(R"(/.*)", hanlder);
    Put(R"(/.*)", hanlder);

    this->_thread = std::make_unique<std::thread>(&HttpServer::listen_after_bind, this);
}

HttpServer* HttpServer::getInstance()
{
    if (_obj == nullptr)
    {
        _obj = new HttpServer();
    }

    return _obj;
}