#pragma once

#include <sqrat.h>
    
namespace HttpBind
{
    void addEvent(const char* eventName);
    void bind();
}