#pragma once

#include <sqrat.h>
#include <httplib.h>

class HttpRequest
{
public:
    std::string _path;

    std::string _addres;
    std::string _method;

    httplib::Headers _headers;
    std::string _body;

public:
    HttpRequest();
    ~HttpRequest();

    static void bind();

    void setPath(const std::string& path);

    const std::string& getPath() const;

    void setAddres(const std::string& addres);

    const std::string& getAddres() const;

    void setMethod(const std::string& method);

    const std::string& getMethod() const;

    void setHeaders(const httplib::Headers& headers);

    const Sqrat::Table getHeaders() const;

    void setBody(const std::string& body);

    const std::string& getBody() const;
};