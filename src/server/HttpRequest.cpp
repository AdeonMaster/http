#include "HttpRequest.h"

#include <iostream>

HttpRequest::HttpRequest() : _addres("localhost:1280"), _method("GET"), _body("{}")
{
}

HttpRequest::~HttpRequest()
{
}

void HttpRequest::bind()
{
    using namespace SqModule;

    /* squirreldoc (class)
    *
    * This class represents http request, is provided only by request callback, values of her objects cannot be modified and containts informations about request.
    *
    * @version	0.0.0
    * @side		shared
    * @name		httpRequest
    * @category server
    *
    */

    Sqrat::Class<HttpRequest> httpRequest(vm, "httpServer_Request");

    httpRequest.Prop("addres", &HttpRequest::getAddres);
    httpRequest.Prop("method", &HttpRequest::getMethod);
    httpRequest.Prop("headers", &HttpRequest::getHeaders);
    httpRequest.Prop("body", &HttpRequest::getBody);
}

void HttpRequest::setPath(const std::string& path)
{
    _path = path;
}

const std::string& HttpRequest::getPath() const
{
    return _path;
}

/* squirreldoc (property)
*
* Through this property, you can get the address from which the request was made.
*
* @version	0.0.0
* @name     addres
* @return   (string) Addres.
*
*/

void HttpRequest::setAddres(const std::string& addres)
{
    _addres = addres;
}

const std::string& HttpRequest::getAddres() const
{
    return _addres;
}

/* squirreldoc (property)
*
* Through this property, you can get the type of the request.
*
* @version	0.0.0
* @name     method
* @return   (string) Request type.
*
*/

void HttpRequest::setMethod(const std::string& method)
{
    _method = method;
}

const std::string& HttpRequest::getMethod() const
{
    return _method;
}

/* squirreldoc (property)
*
* Through tihs property you can get table with headers.
*
* @version	0.0.0
* @name     headers
* @return   (table) Response headers table.
*
*/

void HttpRequest::setHeaders(const httplib::Headers& headers)
{
    _headers = headers;
}

// Squirrel thread
const Sqrat::Table HttpRequest::getHeaders() const
{
    Sqrat::Table sqTable(SqModule::vm);

    for (auto& header : _headers) {
        sqTable.SetValue(header.first.c_str(), header.second.c_str());
    }

    return sqTable;
}

/* squirreldoc (property)
*
* Through tihs property you can get response body.
*
* @version	0.0.0
* @name     body
* @return   (string) Response body.
*
*/

void HttpRequest::setBody(const std::string& body)
{
    _body = body;
}

const std::string& HttpRequest::getBody() const
{
    return _body;
}