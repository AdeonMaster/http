#pragma once

#define CPPHTTPLIB_OPENSSL_SUPPORT

#include <httplib.h>
#include <readerwriterqueue.h>
#include <sqrat.h>
#include <thread>
#include <memory>

#include "json.h"

#include "HttpRequest.h"
#include "HttpResponse.h"

class HttpServer : public httplib::Server
{
private:
    int _port;
    bool _visiblePublic;

    moodycamel::ReaderWriterQueue<HttpRequest> _requestQueue;
    moodycamel::ReaderWriterQueue<HttpResponse> _responseQueue;

    std::unique_ptr<std::thread> _thread;

public:
    HttpServer();
    static HttpServer* _obj;

public:
    void requestHanlder(const httplib::Request& request, httplib::Response& response);

    //void requestListener();
    void responseListener();

    void run(const int config_port, const bool config_visibility);
    static HttpServer* getInstance();
};
