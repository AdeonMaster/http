#include "HttpResponse.h"

HttpResponse::HttpResponse() : _status(404), _body("{}")
{
}

HttpResponse::~HttpResponse()
{
}

void HttpResponse::bind()
{
    using namespace SqModule;

    /* squirreldoc (class)
    *
    * This class represents http response, is provided only by request callback, values of her objects can be modified and containts informations about your reponse to request.
    *
    * @version	0.0.0
    * @side		shared
    * @name		httpResponse
    * @category server
    *
    */

    Sqrat::Class<HttpResponse> httpResponse(vm, "httpServer_Response");

    httpResponse.Prop("status", &HttpResponse::getStatus, &HttpResponse::setStatus);
    httpResponse.Prop("headers", &HttpResponse::getTransformedHeaders, &HttpResponse::setHeaders);
    httpResponse.Prop("body", &HttpResponse::getBody, &HttpResponse::setBody);
}

/* squirreldoc (property)
*
* Through tihs property you can set response status code.
*
* @version	0.0.0
* @name     status
* @return   (int) Response status code.
*
*/

void HttpResponse::setStatus(int status)
{
    _status = status;
}

const int HttpResponse::getStatus() const
{
    return _status;
}

/* squirreldoc (property)
*
* Through tihs property you can set response content type.
*
* @version  0.0.0
* @name     headers
* @return   (table) Response headers.
*
*/

// Squirrel thread
void HttpResponse::setHeaders(const Sqrat::Table& headers)
{
    Sqrat::Object::iterator it;

    while (headers.Next(it))
    {
        Sqrat::string key = pjson::tostring(it.getKey());

        if (key == "")
            continue;

        Sqrat::string value = pjson::tostring(it.getValue());

        if (value == "")
            continue;

        _headers.emplace(key, value);
    }
}

const httplib::Headers& HttpResponse::getHeaders() const
{
    return _headers;
}

// Squirrel thread
const Sqrat::Table HttpResponse::getTransformedHeaders() const
{
    Sqrat::Table sqTable(SqModule::vm);

    for (auto& header : _headers) {
        sqTable.SetValue(header.first.c_str(), header.second.c_str());
    }

    return sqTable;
}

/* squirreldoc (property)
*
* Through tihs property you can set response body content.
*
* @version  0.0.0
* @name     body
* @return   (string) Response body, formatted as text/JSON, depending on used API it`s possible to transform body into array/table.
*
*/

void HttpResponse::setBody(const std::string& body)
{
	_body = body;
}

const std::string& HttpResponse::getBody() const
{
    return _body;
}
