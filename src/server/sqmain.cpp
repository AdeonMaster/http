#define WIN32_LEAN_AND_MEAN

#include <iostream>

#include "HttpServer.h"
#include "HttpBind.h"

#include "sq_callback.h"

#include <json.h>
#include <sqrat.h>
#include <pugixml.hpp>

void addEventHandler(const char* eventName, SQFUNCTION closure, unsigned int priority = 9999)
{
    using namespace SqModule;

    Sqrat::Function sq_addEventHandler = Sqrat::RootTable().GetFunction("addEventHandler");

    if (sq_addEventHandler.IsNull())
        return;

    HSQOBJECT closureHandle;

    sq_newclosure(vm, closure, 0);
    sq_getstackobj(vm, -1, &closureHandle); 

    Sqrat::Function func(vm, Sqrat::RootTable().GetObject(), closureHandle);
    sq_addEventHandler(eventName, func, priority);

    sq_pop(vm, 1);
}

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
    SqModule::Initialize(vm, api);
    Sqrat::DefaultVM::Set(vm);

    Sqrat::Object serverSide = Sqrat::ConstTable(vm).GetSlot("SERVER_SIDE");

    if (serverSide.Cast<bool>())
    {
        addEventHandler("onTick", sq_onTick, 0xFFFFFFFF);
    }
    else
    {
        addEventHandler("onRender", sq_onTick, 0xFFFFFFFF);
    }

    pugi::xml_document document;

    if (document.load_file("http_server.xml"))
    {
        pugi::xml_node configNode = document.child("config");

        if (configNode.empty())
        {
            std::cout << "Http server module cannot find config file - http_server.xml" << std::endl;
            return SQ_ERROR;
        }

        pugi::xml_node publicNode = configNode.child("public");
        pugi::xml_node portNode = configNode.child("port");

        if (!publicNode.empty() && !portNode.empty())
        {
            std::string publicValue = publicNode.child_value();

            try
            {
                HttpServer::getInstance()->run(std::stoi(portNode.child_value()), (std::string(publicValue) == "true") || (std::string(publicValue) == "1"));

                HttpBind::bind();
                // Response/request
                HttpResponse::bind();
                HttpRequest::bind();

                pjson::bind();
            }
            catch (std::exception err)
            {
                std::cout << err.what() << std::endl;
            }
        }
    }

    return SQ_OK;
}
