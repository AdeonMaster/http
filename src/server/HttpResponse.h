#pragma once

#include <sqrat.h>
#include <httplib.h>
#include <json.h>

class HttpResponse
{
public:
    int _status;
    httplib::Headers _headers;
    std::string _body;

public:
    HttpResponse();
    ~HttpResponse();

public:
    static void bind();

    void setStatus(int status);

    const int getStatus() const;

    void setHeaders(const Sqrat::Table& contentType);

    const httplib::Headers& getHeaders() const;

    const Sqrat::Table getTransformedHeaders() const;
   
    void setBody(const std::string& body);

    const std::string& getBody() const;
};