#include "HttpBind.h"

void HttpBind::addEvent(const char* eventName)
{
    Sqrat::Function sq_addEvent = Sqrat::RootTable().GetFunction("addEvent");

    if (sq_addEvent.IsNull())
        return;

    sq_addEvent(eventName);
}

void HttpBind::bind()
{
    using namespace SqModule;

    /* squirreldoc (event)
    *
    * This event is triggered every time when server receives http request.
    *
    * @version	0.0.0
    * @side		shared
    * @name		httpServer:onRequest
    * @category server
    * @param	(string) path Contains request path.
    * @param	(object) httpRequest Object, whitch contains information of request.
    * @param	(object) httpResponse Object, whitch contains information of response, you can use this object to manipulate your response.
    *
    */
    addEvent("httpServer:onRequest");
}