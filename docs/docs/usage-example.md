## Client script example

```js
httpClient.get("api.gothic-online.com.pl/v2/master/server/194.163.165.112:12785/online-history/");

addEventHandler("httpClient:onResponse", function(response) 
{
    local pack = pjson.unpackJSON(response.body);
    
    foreach(index, object in pack)
    {
        print(index + " " + object);

        if(index == "history")
        {
            foreach(value in object)
            {
                print(value.count + " " + value.day);
            }
        }
    }
});

// Example with headers, last argument is header table - key and value

local body = {
	"TEST": "TEST",
};

httpClient.post("eof5fq76pw07478.m.pipedream.net", "application/json", pjson.packJSON(body), {
	"X-API-Key": "TEST1",
	"X-API-Key_2": "TEST2",
});
```

## Server config example

Your server config should be placed in file named - http_server.xml

```xml
<config>
    <public>false</public>
    <port>8080</port>
</config>
```

```
Public - determines whether your HTTP server can be reached from outside your internal network.
```

```
Port - specifies the port on which your HTTP server is going to operate.
```

## Server script example

```js
addEventHandler("httpServer:onRequest", function (path, request, response) 
{
    // List all headers
	foreach(key, value in request.headers) {
		print(key + " " + value)
	}

    switch(path)
    {
        case "/status": 
            response.status = 200;
            response.headers = {
				"Content-Type": "application/json"
			};

            response.body = pjson.packJSON({ count = getPlayersCount(), max_slots = getMaxSlots() });
        break;
    }
});
```

